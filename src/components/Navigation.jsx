import "./Navigation.css";

function Navigation() {
  return (
    <ul className="Navigation">
      {/* TODO add links to the different pages */}
      <li>Home</li>
      <li>Products</li>
      <li>Cart</li>
    </ul>
  );
}

export default Navigation;
