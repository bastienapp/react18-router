import React from "react";
import ReactDOM from "react-dom/client";

// TODO install and import react router

import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* TODO use react router here */}
    <Home />
    <ProductList />
    <Cart />
  </React.StrictMode>
);
