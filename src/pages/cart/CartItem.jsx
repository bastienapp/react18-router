import PropTypes from "prop-types";

function CartItem(props) {
  const { title, quantity } = props;
  return (
    <li className="CartItem">
      <h3>{title}</h3>
      <p>Quantity: {quantity}</p>
    </li>
  );
}

CartItem.propTypes = {
  title: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,
};

export default CartItem;
