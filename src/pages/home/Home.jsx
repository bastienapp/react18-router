import Navigation from "../../components/Navigation";

function Home() {
  return (
    <>
      <Navigation />
      <div className="Home">
        <h1>Home page</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae
          sed nulla fugit quaerat non error omnis saepe eligendi ipsam rerum
          voluptatem mollitia nostrum, assumenda doloribus? Unde facilis sunt
          eaque reiciendis.
        </p>
      </div>
    </>
  );
}

export default Home;
